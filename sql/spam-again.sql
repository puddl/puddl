-- total
SELECT
    count(*)
FROM xs
;

-- since yesterday
SELECT
    subject,
    count(*)
FROM xs
WHERE dt > 'yesterday'
GROUP BY subject
ORDER BY count(*) DESC
;

-- group by similar subject
CREATE EXTENSION IF NOT EXISTS pg_trgm
;
CREATE INDEX IF NOT EXISTS ix_xs_subject ON xs USING gin (subject gin_trgm_ops)
;

-- https://stackoverflow.com/a/11250001/241240
SET pg_trgm.similarity_threshold = 0.6
;

-- duplicates
WITH x AS (
    -- cross product with pair-wise similarity
    SELECT
        similarity(a.subject, b.subject) AS sim,
        a.subject AS a_subject,
        b.subject AS b_subject,
        a.key AS a_key,
        b.key AS b_key
    FROM xs a
             JOIN xs b ON a.key <> b.key AND a.subject % b.subject
    WHERE a.dt > 'yesterday'
        -- get the first mail delivered as "original" down the line
    ORDER BY a.dt ASC
)
SELECT
    avg(sim) AS sim,
    a_subject,
    array_agg(b_subject) AS dup_subjects,
    a_key,
    array_agg(b_key) AS dup_keys
FROM x
GROUP BY a_key, a_subject
ORDER BY sim DESC
;
