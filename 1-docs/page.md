---
version: draft
---

> All that happens must be known [^the-circle]

... **to thyself**!

[^the-circle]: https://en.wikipedia.org/wiki/The_Circle_(Eggers_novel) -- read
  the book; skip the movie.


# What is this?
This is your **Personal**
[Data Lake](https://en.wikipedia.org/wiki/Data_lake) aka. **puddl**.

Imagine taking videos of all the things you do. All this data has to live
somewhere: puddl! [^moar]

Jotting down ideas? Talk to the camera!

Having a bad day? Talking helps!

Having fun? Share it!


[^moar]: ... not just videos; ALL **your** data


# Privacy
Your data is **yours**.

- puddl is local first
- puddl is NAS second
- puddl is private server next
- puddl is cloud last


# Vision
- zeitliche korrelation ist zentral
  - video stream mit TS
  - sprache zu kürzel
  - musik hören
  - mail schreiben
  - commits over time
- videos in TOPICS segmentieren (statt Gedankengang)


# puddl-timebox
Usage:
```
puddl-timebox 40
```

- creates a timebox of 40 minutes based on the last `puddl-hut` entry
- shows big screen (with QR code?) like `timebox 40 minutes: fooproject/dev some comment`
- reminds a few minutes before end (e.g. 1/8th, because 5 minutes are nice for a
  40 minutes timebox)
- flashes something red and big and flashy on reaching the timebox


# Notes (aka. Sortme)
- Development is interest- and mood-driven. Feel like hacking on puddl? Go
  ahead!
- Need support? Ask nicely [^netiquette] and you *might* be heard. No promises though.

[^netiquette]: https://tools.ietf.org/html/rfc1855


