# No "continuous form" - this makes matching easier.
Good:

- work
- chill with

Bad:

- working
- chilling with
