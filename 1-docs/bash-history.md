So we got something that puts lines of bash history into the database. Seems
rather NIH Syndrome to me...

... because it is: https://stackoverflow.com/a/25586279/241240

Also nice: plot your zsh history: https://github.com/bamos/zsh-history-analysis
