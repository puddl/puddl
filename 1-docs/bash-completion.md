# Bash Completion
```
help complete
help compgen
man -P 'less -p "complete -pr"' bash
```

list all commands beginning with `puddl-`:
```
compgen -A command puddl-
```
