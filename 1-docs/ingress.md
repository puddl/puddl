---
version: draft
---

# Ingress
Status: hopes and dreams

- git commits
  - hook --> stream(device, timestamp, diff)
- video files
  - streamlet audio2text
  - streamlet video2text with bounding boxes
- [OBS](https://obsproject.com/) streams
- [Action Per Minute Counter](https://github.com/felixhummel/x-apm-timescaledb)
- time tracker
- Gitlab
- Github
- local git repos
  - shell hook to annotate "created by me"
- google sheets, e.g. "preisvergleich"
- eye-tracking
  - currently "looked at" word to shell
- mail to puddl@example.com containing a single URL
  - body gets saved and linked to the URL
  - next mail's body too
  - if you find the URL you get a list of bodies (ORDER BY dt DESC)
- physical location
  - you leave your workstation --> timetracking pauses
