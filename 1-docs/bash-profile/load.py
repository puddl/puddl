#!/usr/bin/env python

import sys
from pathlib import Path

import pandas as pd

from puddl.pg import DB

fd = Path(sys.argv[1]).expanduser().open()
data_iter = (line.rstrip("\n") for line in fd)
df = pd.DataFrame(data_iter, columns=["line"])

db = DB("bash_profile")
db.df_dump(df, "lines", drop_cascade=True)
