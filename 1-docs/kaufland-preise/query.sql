WITH xs AS (
    SELECT
        title,
        -- replace(substring(price FROM '[\d,]+'), ',', '.')::NUMERIC AS price,
        replace(substring("base-price" FROM '1kg = ([\d,]+)'), ',', '.')::NUMERIC AS kilopreis
    FROM knoblauchpulver
)
SELECT *
FROM xs
WHERE kilopreis IS NOT NULL
ORDER BY kilopreis
;
