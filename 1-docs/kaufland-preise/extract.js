function t(x) {
  if (x) {
    return x.textContent.trim();
  } else {
    return '';
  }
}

ps = Array.from(document.querySelectorAll('.product'))
xs = ps.map((p) => {
  return {
    'title': t(p.querySelector('.product__title')),
    'price': t(p.querySelector('.price')),
    'base-price': t(p.querySelector('.product__base-price > span')),
  }
});

result = JSON.stringify(xs, null, 2);
console.log(result)

// needs permission
// navigator.clipboard.writeText(result);

// just works
copy(result)
