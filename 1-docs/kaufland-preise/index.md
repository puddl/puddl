# Kaufland Preise
![](https://gitlab.com/puddl/puddl/-/wikis/uploads/f23a459d8edfadd756e0d1fa73d95ef0/image.png)

1. Firefox Console, run [extract.js](extract.js).
2. Load to DB via `ipython`
```
import pandas as pd
from puddl.pg import DB

!clipboard > /tmp/knoblauchpulver.json
db = DB('kaufland')
df = pd.read_json('/tmp/knoblauchpulver.json')
db.df_dump(df, 'knoblauchpulver')
```

Grafana

- new panel
- Bar chart
- Format as: Table
- paste [query.sql](query.sql)
- unit: €
