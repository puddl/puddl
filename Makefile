MAKEFLAGS += --always-make

default: code-style test

DANGER_remove_everything:
	./env/dev/DANGER_destroy_compose_environment.sh
	rm .env

code-style:
	./env/dev/git-hooks/pre-commit

test:
	puddl-db health
	pytest
	puddl-db health >/dev/null
	puddl-db ls >/dev/null
	puddl-config show >/dev/null

e2e-setup-py:
	./env/dev/bin/e2e-setup-py.sh

build:
	python setup.py sdist bdist_wheel

release-test-pypi: clean code-style test build
	twine upload --repository testpypi dist/*

release-pypi:
	twine upload --repository pypi dist/*

release: clean test build release-pypi

e2e-pypi:
	./env/dev/bin/e2e-pypi.sh

clean:
	rm -rf build/ dist/

db_reload:
	docker-compose exec -u postgres db pg_ctl reload

db_logs:
	docker-compose logs --tail=50 -f db
