#!/usr/bin/env python
import sys
from datetime import datetime
from itertools import islice
from mailbox import Maildir, MaildirMessage
from pathlib import Path
from typing import Optional

import pandas as pd
import structlog
from pydantic import BaseModel

from puddl.pg import DB

log = structlog.get_logger()

db = DB('spam')
p = Path(sys.argv[1]).expanduser()
d = Maildir(p)


# I like Pydantic models. In this case, we can put unix timestamps in the dt field and get
# sanitized datetime objects. \o/
class Mail(BaseModel):
    key: str
    dt: datetime
    to: Optional[str] = None
    subject: Optional[str] = None

    @staticmethod
    def _str_or_none(m: MaildirMessage, k: str):
        v = m.get(k)
        if v is not None:
            return str(v)
        return None

    @classmethod
    def parse_mailbox(cls, k: str, m: MaildirMessage):
        return cls(
            key=k,
            dt=m.get_date(),
            to=cls._str_or_none(m, 'to'),
            subject=cls._str_or_none(m, 'subject'),
        )


# items = islice(d.iteritems(), 50)
items = islice(d.iteritems(), None)
xs = [Mail.parse_mailbox(k, m) for k, m in items]
df = pd.DataFrame([x.model_dump() for x in xs])
db.df_dump(df, 'xs')
