# Idea: Enforce App Schema Usage via Grants
Motivation: prevent programming errors like `TRUNCATE history` instead of
`TRUNCATE codimd.history`

Example:
The `codimd` app uses `get_alchemy_session` and the model `Remote`. Since
`Remote` is based on `Schema('codimd').declarative_base`, the `search_path` is
set correctly.
Later on it executes `TRUNCATE codimd.history RESTART IDENTITY CASCADE`. If we
forgot to specify the schema then we could possibly truncate a `history` table
in the `public` schema.

Better:
In order to enforce the schema usage of every app it is given a `ROLE` with
explicit `GRANT`s on the schema. Credentials are generated and persisted when
running `app add`.


# What do apps need know?
- name (default = last part of module) as schema
  - `puddl.conf.Config.add_app()` must make sure that there are no conflicts
- custom psql session name
  - see at a glance what's going on with `puddl db sessions`
  - convention: prefixed with app name, e.g.
    - `codimd query`
    - `file indexer`

## App Name
Keeping it simple and conservative:
```
RE_APPNAME = r'[a-z_][0-9a-z_]?'
```

The app name must be both a valid SQL identifier as well as a valid python
module name. We further exclude dollar signs, because they are ugly.

> SQL identifiers and key words must begin with a letter (a-z, but also letters
> with diacritical marks and non-Latin letters) or an underscore (`_`).
> Subsequent characters in an identifier or key word can be letters,
> underscores, digits (0-9), or dollar signs ($).

https://www.postgresql.org/docs/current/sql-syntax-lexical.html#SQL-SYNTAX-IDENTIFIERS

> Modules should have short, all-lowercase names. Underscores can be used in the
> module name if it improves readability.

https://www.python.org/dev/peps/pep-0008/#package-and-module-names
