# puddl file q
```
SELECT id, path
FROM file_file
WHERE array_to_tsvector(regexp_split_to_array(trim(LEADING '/' FROM path), '/')) @@ to_tsquery('puddl & setup.py')
;
```

- https://docs.djangoproject.com/en/3.0/ref/contrib/postgres/search/
- https://www.postgresql.org/docs/current/functions-string.html
- https://www.postgresql.org/docs/current/functions-textsearch.html
- https://stackoverflow.com/questions/8907041/django-postgresql-regexp-split-to-table-not-working
- https://docs.djangoproject.com/en/3.0/ref/models/querysets/
- https://docs.djangoproject.com/en/3.0/ref/models/database-functions/
- https://docs.djangoproject.com/en/3.0/topics/db/sql/
- https://www.postgresql.org/docs/current/textsearch-controls.html
- https://docs.djangoproject.com/en/3.0/ref/models/expressions/


OK. This was a nice experiment, but let's continue with the simple and stupid
approach.  :>
