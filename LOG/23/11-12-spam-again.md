# Spam Again
A different approach from [][LOG/01-12.md]:

Get all mail's date, "to" and "subject":
```
./bin/mail-load.py ~/puddl/spam
```

Find subjects that are duplicates based on the other subjects:
```
psql $(puddl-db url spam) < sql/spam-again.sql
```
