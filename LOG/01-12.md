# Correlating Domain Prefixes with Spam E-Mail
I use different email addresses to sign up for various services, by simply
changing the name before the `@` sign in my email addresses [^1], for example:

- spam@felixhummel.de
- 764efa883dda1e11db47671c4a3bbd9e@felixhummel.de
- fooservice@felixhummel.de

This is one of the benefits of owning a personal domain.

I also get lots of spam. Let's see how much spam my different mail accounts
receive.
```
ls /tmp/Spam
# It's a Maildir containing cur, new and tmp

./bin/mail-spam-per-addr.py /tmp/Spam | sort | uniq -c | sort -nr
```

## Analysis
Top 5
```
   4763 dropbox
   4096 felix
    874 tele
    673 rappelz
    310 apache
```
Well, `dropbox` seems to have been hacked once or twice. :D
The `felix`-Prefix is simply too present on the net (it's my Git author address).
Both `tele` and `rappelz` belong to services that are long gone.
I used `apache` for jira@apache.org, which is publicly available, so it's no
wonder that spammers know of this. Who decides to scrape Apache's Jira for
verified email addresses though? Well played.

At the bottom of this list are many entries with just a single hit. Some
commented examples:
```
      1 kde-look  # cute
      1 ja2  # good old jagged alliance 2 forum
      1 epicgames  # mhm.. epic got hacked? ^^
      1 cudnn  # nvidia, I'm looking at you :>
      1 4f522be5c  # wtf? paste fail or some kind of enumeration attempt?
```

[^1]: The host name historically speaking.
