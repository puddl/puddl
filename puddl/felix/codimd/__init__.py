#!/usr/bin/env python
from puddl.felix.codimd.cli import main  # noqa: F401

__version__ = '0.1.1'


class CodiMDError(Exception):
    pass
