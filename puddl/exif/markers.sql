DROP TABLE IF EXISTS markers
;

CREATE TABLE markers AS
WITH a AS (
    SELECT
        index,
        _name AS name,
        _path,
        exif_coords2float_coords("GPS GPSLatitude") AS lat,
        exif_coords2float_coords("GPS GPSLongitude") AS lng,
        coalesce("Image GPSInfo"::INT, 400) AS alt,
        -- We parse the datetime to make sure it is interpreted without timezone and make it a text
        -- just to be able to concatenate it with the given offset.
        -- Then we cast it with time zone again.
        (
                    to_timestamp("EXIF DateTimeOriginal", 'YYYY:MM:DD HH24:MI:SS')::TIMESTAMP WITHOUT TIME ZONE::TEXT
                    || ' ' || coalesce("EXIF OffsetTime", '+02:00')
            )::TIMESTAMP WITH TIME ZONE
            AS dt,
        thumb,
        CASE
            WHEN "Image Orientation" IS NULL THEN 0
            WHEN "Image Orientation" = '0' THEN 0
            WHEN "Image Orientation" = 'Horizontal (normal)' THEN 0
            WHEN "Image Orientation" = 'Rotated 90 CW' THEN 90
            WHEN "Image Orientation" = 'Rotated 180' THEN 180
            WHEN "Image Orientation" = 'Rotated 90 CCW' THEN 270
        END AS rotation
    FROM s7
    WHERE _name IS NOT NULL
        AND "GPS GPSLatitude" IS NOT NULL
        AND "GPS GPSLongitude" IS NOT NULL
)
SELECT
    index AS id,
    name,
    'file://' || _path AS url,
    lat,
    lng,
    alt,
    timestamp_to_iso8601(dt) AS dt,
    dt AS dt_ts,
    thumb,
    rotation,
    (dt::timestamptz - (
        SELECT
            min(dt)::timestamptz
        FROM a
    ))::TEXT AS since_start
FROM a
;
