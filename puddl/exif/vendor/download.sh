#!/bin/bash
set -euo pipefail

cd "$(dirname "$0")"

wget -qc https://moment.github.io/luxon/global/luxon.min.js
wget -qc https://unpkg.com/leaflet@1.7.1/dist/leaflet.js
wget -qc https://unpkg.com/leaflet@1.7.1/dist/leaflet.js.map
wget -qc https://unpkg.com/leaflet@1.7.1/dist/leaflet.css
wget -qc https://cdnjs.cloudflare.com/ajax/libs/leaflet.markercluster/1.5.3/leaflet.markercluster.js
wget -qc https://cdnjs.cloudflare.com/ajax/libs/leaflet.markercluster/1.5.3/leaflet.markercluster.js.map
wget -qc https://cdnjs.cloudflare.com/ajax/libs/leaflet.markercluster/1.5.3/MarkerCluster.min.css
wget -qc https://cdnjs.cloudflare.com/ajax/libs/leaflet.markercluster/1.5.3/MarkerCluster.Default.min.css
wget -qc https://cdn.plot.ly/plotly-2.4.2.min.js
wget -qc https://d3js.org/d3.v7.min.js
wget -qc https://raw.githubusercontent.com/bbecquet/Leaflet.RotatedMarker/master/leaflet.rotatedMarker.js
