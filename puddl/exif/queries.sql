-- be sure to run definitions.sql first
SELECT exif_coords2float_coords('[47, 51, 38]')
;

SELECT *
FROM markers
;

SELECT
    year,
    month,
    count(*)
FROM jpg_date
GROUP BY year, month
;

SHOW TIMEZONE
;
SELECT current_timestamp
;

-- rotation
SELECT
    "Image Orientation",
    count(index)
FROM s7
GROUP BY "Image Orientation"
ORDER BY count(index) DESC
;
