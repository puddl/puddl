from pathlib import Path
from typing import Iterable

import pandas
import pandas as pd
from git import Repo

from puddl.git.utils import list_repo_paths


def iter_git_paths_recursive(*paths: Path):
    """
    Find Repos in paths recursively.
    """
    for path in paths:
        yield from list_repo_paths(path.expanduser())


def commits(repo: Repo, revision='--all') -> pd.DataFrame:
    df = pd.DataFrame(repo.iter_commits(revision), columns=['raw'])
    df['_path'] = Path(repo.working_dir)
    return df


def iter_dfs(*repos: Repo) -> Iterable[pd.DataFrame]:
    for repo in repos:
        yield commits(repo)


def dump(*paths):
    repos = [Repo(p) for p in iter_git_paths_recursive(*paths)]
    return pandas.concat(iter_dfs(*repos))
