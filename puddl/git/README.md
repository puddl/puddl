# Usage
```
puddl git duckdb
```
Writes history of *all* commits in *any* repo under the current working directory to a
duckdb database `repos.duckdb`.



# Alternatives
- [git-pandas](https://wdm0006.github.io/git-pandas/#) Didn't use, because there was no "get all commits" in
