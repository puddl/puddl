from pathlib import Path

import pytest
from git import Repo


@pytest.fixture
def empty_root_dir(tmp_path):
    return Path(tmp_path / 'root')


@pytest.fixture
def repo_empty(empty_root_dir):
    return Repo.init(empty_root_dir / 'repo_empty')


@pytest.fixture
def repo_code(empty_root_dir):
    p = empty_root_dir / 'repo_code'
    r = Repo.init(p)
    hello = p.joinpath('hello.py')
    hello.write_text('print("hello world")')
    r.index.add(str(hello))
    r.index.commit("initial commit")
    return r


@pytest.fixture
def root_dir(empty_root_dir, repo_empty, repo_code) -> Path:
    # The creation of the repos should be more explicit here...
    return Path(empty_root_dir)


@pytest.fixture
def repo_puddl():
    return Repo(__file__, search_parent_directories=True)
