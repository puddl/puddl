from pathlib import Path

import git

from puddl.git.api import commits, iter_git_paths_recursive, dump


def test_iter_git_paths_recursive(root_dir, repo_puddl):
    paths = list(
        iter_git_paths_recursive(root_dir, Path(repo_puddl.working_dir))
    )
    assert len(paths) == 3


def test_commits(repo_puddl):
    df = commits(repo_puddl)
    assert df.size >= 470
    commit = df.head(1)['raw'][0]
    assert isinstance(commit, git.Commit)


def test_dump(root_dir):
    df = dump(root_dir)
    assert len(df) == 1
    d = df.head(1).to_dict('records')[0]
    assert isinstance(d['raw'], git.Commit)
    assert isinstance(d['_path'], Path)
